# sre-test

### Introduction
Just for SRE code test.

### how to use
1. run ` mvn clean package ` on local to create the .jar
2. run ` docker build . -t sre-test:v1 -f Dockerfile ` to build the image 
3. run ` docker run --rm --name sre-test sre-test:v1`
4. run ` curl http://localhsot:8800/xjming `